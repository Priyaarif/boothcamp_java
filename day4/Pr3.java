package day4;

import java.util.Scanner;

public class Pr3{
	
	public static void main(String[] args){
		Pr3 menggambar = new Pr3();
		menggambar.looping();
	}
	
	public int nilaiN(){
		Scanner data = new Scanner(System.in);
		System.out.print("masukan nilai n : ");
		int n = data.nextInt();
		return n;
	}

	public int nilaiX(){
		Scanner data = new Scanner(System.in);
		System.out.print("masukan cara yang ingin digunakan : (1/2) ");
		int x = data.nextInt();
		return x;
	}
	
	public int nilaiY(){	
		Scanner data = new Scanner(System.in);
		System.out.print("masukan pola yang diinginkan : ");
		int y = data. nextInt();
		return y;
	}
	
	public void gambar(int n, int y){
		for (int i = 0; i < n ; i++){
			for (int j = 0; j < n ; j++){
				if (i % 2 == 0 && j < y){
					System.out.print("*");
				} else if (i % 2 == 1 && j >= n-y){
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

	public void gambar1(int n, int y){
		for (int i = 0; i < n ; i++){
			for (int j = 0 ; j < n ; j++){
				if ( y == 1){
					if (i % 2 == 0 && j < n/2){
						System.out.print("*");
					} else if (i % 2 == 1 && j >= n/2){
						System.out.print("*");
					} else {
						System.out.print(" ");
					}
				} else if ( y == 2){
					if (i % 2 == 0 && j < n/2){
						System.out.print("*");
					} else if (i % 2 == 1 && j > n/2){
						System.out.print("*");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}

	public void looping(){
		Pr3 menggambar = new Pr3();
		Scanner data = new Scanner(System.in);
		String loop = null;
		do {
			int x = menggambar.nilaiX();
			int n = menggambar.nilaiN();
			int y = menggambar.nilaiY();
			switch (x){
				case 1 :
					menggambar.gambar(n,y);
					break;
				case 2 :
					menggambar.gambar1(n,y);
					break;
				default :
					System.out.println("cara yang ingin digunakan tidak terdaftar, apakah ingin berhenti? (y/n)");
					break;
			}
			System.out.println("apakah ingin berhenti? (y/n)");
			loop = data.nextLine();
		} while (loop.equals("n"));
	}
}
	