package day4;

import java.util.Scanner;

public class Tugas1 {

    public static void main(String[] args) {
        int n;
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        n = data.nextInt();
        Tugas1 coba = new Tugas1();
        coba.gambar(n);
    }

    public void gambar(int n){
        char x = 'A';
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i <= j){
                    if (i % 2 == 0){
                        System.out.printf("%3s",x);
                    } else {
                        System.out.printf("%3s",(int)x);
                    }
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
            x++;
        }
    }
}
