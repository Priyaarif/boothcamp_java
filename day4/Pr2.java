package day4;

import java.util.Scanner;

public class Pr2{
	
	public static void main(String[] args){
		Pr2 menggambar = new Pr2();
		menggambar.looping();
	}
	
	public void looping(){
		Pr2 menggambar = new Pr2();
		Scanner data = new Scanner(System.in);
		String pilihan = null;
		do {
			int n = menggambar.Nilain();
			menggambar.gambar(n);
			System.out.println("apakah ingin berhenti? (y/n) ");
			pilihan = data.nextLine();
		} while (pilihan.equals( "n" ));
	}
	
	public int Nilain(){
		Scanner data = new Scanner(System.in);
		System.out.print("masukan nilai n : ");
		int n = data.nextInt();
		return n;
	}
	
	public void gambar(int n){
		for (int i = 0; i < n ; i++){
			for (int j = 0; j < n ; j++){
				if (i % 2 == 1 && j>=n-i-1){
					System.out.print("*");
				} else if ( i % 2 == 0 && j <= i ){
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}