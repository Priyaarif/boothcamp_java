package day4;

import java.util.Scanner;

public class Tugas2 {

    public static void main(String[] args) {
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        Tugas2 menggambar = new Tugas2();
        menggambar.gambar(n);
    }

    public void gambar(int n){
        char x = 'A';
        char y = 'A';
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i==j || i+j==n-1){
                    System.out.print("*");
                } else if (i<j && i+j<n){
                    System.out.print(x);
                } else if (i>j && i+j>=n){
                    System.out.print(x);
                } else if (i<j && i+j>=n){
                    System.out.print(--y);
                } else if (i>j && i+j<n){
                    System.out.print(y++);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if (n%2==1) {
                if (i < n / 2) {
                    x++;
                } else {
                    x--;
                }
            } else {
                if (i < (n / 2)-2) {
                    x++;
                } else if (i > n/2){
                    x--;
                }
            }
        }
    }
}
