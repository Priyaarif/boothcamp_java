package kuis;

import java.util.Scanner;

public class Kuis3 {

    public int getN(){
        Scanner s = new Scanner(System.in);
        return s.nextInt();
    }

    public int[] getArrGanjil(int n){
        int [] arrGanjil = new int[n];
        for (int i = 0 ; i < n ; i++){
            if(i <=  n/2){
                arrGanjil[i] = i * 2 + 1;
            } else {
                arrGanjil[i] = arrGanjil[n - i - 1];
            }

        }
        return arrGanjil;
    }

    public char[] getArrHuruf(int n){
        char [] arrChar = new char[n];
        char c = 'A';
        for (int i = 0 ; i < n ; i++){
            if(i <=  n/2){
                arrChar[i] = c++;
            } else {
                arrChar[i] = arrChar[n - i - 1];
            }

        }
        return arrChar;
    }

    public void cetak(){
        int n = getN();
        int [] arrGanjil = getArrGanjil(n);
        char [] arrHuruf = getArrHuruf(n);
        for (int i = 0 ; i < n ; i++){
            for (int j = 0 ; j < n ; j++){
                if(j<=i){
                    if( i % 2 == 0){
                        System.out.printf("%3d",arrGanjil[j]);
                    } else {
                        System.out.printf("%3s",arrHuruf[j]);
                    }
                }


            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Kuis3 k = new Kuis3();
        k.cetak();
    }
}
