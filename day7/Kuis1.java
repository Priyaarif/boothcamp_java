package day7;

import java.util.Scanner;

public class Kuis1 {


    public int getN(){
        Scanner data = new Scanner((System.in));
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public void k1(){
        int n = getN();
        int x = 0;
        do {
            for (int i = 0; i < x ; i++){
                for (int j = 0; j < x ; j++){
                    if (i >= j){
                        System.out.printf("%3s","X");
                    } else {
                        System.out.printf("%3s"," ");
                    }
                }
                System.out.println();
            }
            x++;
        } while (x <= n);
    }


    public static void main(String[] args) {
        Kuis1 kuis = new Kuis1();
        kuis.k1();
    }
}
