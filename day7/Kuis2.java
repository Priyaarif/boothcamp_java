package day7;

import java.util.Arrays;
import java.util.Scanner;

public class Kuis2 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public int[] getX(int n){
        int[] x = new int[n+1];
        for (int i = 1; i < n + 1 ; i++){
            x[i] = x[i-1] + i ;
        }
        return x;
    }

    public String[][] pola(int[] x,int  n){
        String[][] p = new String[x[n]][n];
        int y = 0;
        do {
            for (int i = 0; i < x[y]; i++) {
                for (int j = 0; j < n; j++) {
                    if (i >= j) {
                        p[x[y]+i][j] = "X";
                    } else if (p[i][j] == null){
                        p[i][j] = "0";
                    }
                }
            }
            y++;
        } while ( y < n);
        return p;
    }

    /*public void segitiga(int n){
        int[][] p =
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i >= j) {
                    p[i][j] = "X";
                } else if (p[i][j] == null){
                    p[i][j] = "0";
                }
            }
        }
    }*/

    public void print(){
        int n = getN();
        int[] x = getX(n);
        String[][] p = pola(x,n);
        for (int i = 0 ; i < n ; i++){
            for (int j = 0; j < x[n] ; j++){
                System.out.printf("%3s",p[j][i]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Kuis2 kuis = new Kuis2();
        int n = kuis.getN();
        int[] x = kuis.getX(n);
        String[][] s = kuis.pola(x,n);
        System.out.println(n);
        System.out.println(Arrays.toString(x));
        System.out.println(Arrays.deepToString(s));
        kuis.print();
    }
}
