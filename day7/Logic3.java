package day7;

import java.util.Arrays;
import java.util.Scanner;

public class Logic3 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public int getSoal(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan soal yang dipilih : ");
        int y = data.nextInt();
        return y;
    }

    public char getChar(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan char a : ");
        char a = data.next().charAt(0);
        return a;
    }

    public int[] fibonacci1(int n){
        int[] f = new int[n];
        f[0] = f[1] = 1;
        for (int i = 0; i < n-2 ; i++){
            f[i+2]=f[i]+f[i+1];
        }
        return f;
    }

    public int[] fibonacci2(int n){
        int[] f = new int[n];
        f[0] = f[1] = f[2] = 1;
        for (int i = 0; i < n-3 ; i++){
            f[i+3]=f[i]+f[i+1]+f[i+2];
        }
        return f;
    }

    public char[] alphabet(int n,char a){
        char[] b = new char[n];
        for (int i = 0; i < n ; i++){
            b[i] = a++;
        }
        return b;
    }

    public void soal3(int n){
        int[] f = fibonacci1(n);
        for (int i = 0; i < n ; i++){
            if (i < n/2){
                System.out.printf("%3s",f[i]);
            } else {
                System.out.printf("%3s",f[n-i-1]);
            }
        }
        System.out.println();
    }

    public void soal4(int n){
        int[] f = fibonacci2(n);
        for (int i = 0; i < n ; i++){
            if (i < n/2){
                System.out.printf("%3s",f[i]);
            } else {
                System.out.printf("%3s",f[n-i-1]);
            }
        }
        System.out.println();
    }

    public void soal5(int n){
        int[] f = fibonacci1(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i > j && i+j < n-1){
                    System.out.printf("%3s"," ");
                } else {
                    System.out.printf("%3s",f[j]);
                }
            }
            System.out.println();
        }
    }

    public void soal6(int n, char a){
        int[] f = fibonacci1(n);
        char[] b = alphabet(n,a);
        int x = 0;
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i == j ){
                    System.out.printf("%3s",f[j]);
                } else if ( i+j == n-1 ){
                    System.out.printf("%3s",f[j]);
                } else if ( i<j && i+j < n-1 ){
                    System.out.printf("%3s",b[x]);
                } else if ( i<j && i+j >= n-1 ){
                    System.out.printf("%3s",b[x+1]);
                } else if ( i>j && i+j >= n-1 ){
                    System.out.printf("%3s",b[x+2]);
                } else if ( i>j && i+j < n-1){
                    System.out.printf("%3s",b[x+3]);
                }
            }
            System.out.println();
        }
    }

    public void soal7(int n){
        int[] f = fibonacci1(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i <= j && i+j <= n-1 ){
                    System.out.printf("%3s",f[i]);
                } else if ( i <= j && i+j >= n-1 ){
                    System.out.printf("%3s",f[n-j-1]);
                } else if( i >= j && i+j >= n-1){
                    System.out.printf("%3s",f[n-i-1]);
                } else if ( i >= j && i+j <= n-1){
                    System.out.printf("%3s",f[j]);
                }
            }
            System.out.println();
        }
    }

    public void soal8(int n){
        int[] f = fibonacci1(n);
        for (int i = 0 ; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i <= j && i+j <= n-1 && i % 2 == 0){
                    System.out.printf("%3s",f[i]);
                } else if ( i <= j && i+j >= n-1 && j % 2 == 0 ){
                    System.out.printf("%3s",f[n-j-1]);
                } else if( i >= j && i+j >= n-1 && i % 2 == 0){
                    System.out.printf("%3s",f[n-i-1]);
                } else if ( i >= j && i+j <= n-1 && j % 2 == 0){
                    System.out.printf("%3s",f[j]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal9(int n){
        int[] f = fibonacci1(n);
        int x = 0;
        int y = 0;
        for (int i = 0 ; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i <= j && i+j <= n-1 && i % 2 == 0){
                    System.out.printf("%3s",f[x]);
                } else if ( i <= j && i+j >= n-1 && j % 2 == 0 ){
                    System.out.printf("%3s",f[n-y-1]);
                } else if( i >= j && i+j >= n-1 && i % 2 == 0){
                    System.out.printf("%3s",f[n-x-1]);
                } else if ( i >= j && i+j <= n-1 && j % 2 == 0){
                    System.out.printf("%3s",f[y]);
                } else {
                    System.out.printf("%3s"," ");
                }
                x++;
            }
            System.out.println();
            y++;
        }
    }

    public void pilihan(int y){
        int n = getN();
        switch (y){
            case 1 :
                System.out.println(Arrays.toString(fibonacci1(n)));
                break;
            case 2 :
                System.out.println(Arrays.toString(fibonacci2(n)));
                break;
            case 3 :
                soal3(n);
                break;
            case 4 :
                soal4(n);
                break;
            case 5 :
                soal5(n);
                break;
            case 6 :
                char a = getChar();
                soal6(n,a);
                break;
            case 7 :
                soal7(n);
                break;
            case 8 :
                soal8(n);
                break;
            case 9 :
                soal9(n);
                break;
            default :
                System.out.println("Soal yang anda masukan belum terdaftar");
                break;
        }
    }

    public void looping(){
        Scanner data = new Scanner(System.in);
        String loop = null;
        do {
            int y = getSoal();
            pilihan(y);
            System.out.println("apakah ingin berhenti? (y/n) ");
            loop = data.nextLine();
        } while (loop.equals("n"));
    }

    public static void main(String[] args) {
        Logic3 logic = new Logic3();
        logic.looping();
    }
}
