package day3;

public class BelajarMethod3 {

    public void cetak1(){
        BelajarMethod2 belajar = new BelajarMethod2(); //membuat objek
        String nama = belajar.nama();
        int umur = belajar.umur();
        System.out.println("nama saya adalah "+nama);
        System.out.print("umur saya "+umur+" tahun");
    }

}
