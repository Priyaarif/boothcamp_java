package day3;

import java.util.Scanner;

public class BelajarMethod {

    public int getB(){
        Scanner data = new Scanner(System.in);
        int n = data.nextInt();
        return n;
    }

    public void cetak(){
        int n = getB();
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print("*");
                if (i < n/2){
                    if (n+i < j && j < n-i){
                        System.out.print("*");
                    }
                    else {
                        System.out.print(" ");
                    }
                }
                else {
                    if (j < n-i || j > i){
                        System.out.print(" ");
                    }
                    else {
                        System.out.print("*");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args){
        BelajarMethod yukDi = new BelajarMethod();
        System.out.print("masukan nilai n : ");
        yukDi.cetak();
    }
}
