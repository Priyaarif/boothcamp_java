package day3;

import java.util.Scanner;

public class BangunDatar {
    public void cetak(){
        int n = getN();
        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public int getN(){
        Scanner data = new Scanner(System.in);
        int n = data.nextInt();
        return n;
    }
    public static void main(String[] args){
        BangunDatar bangunDatar = new BangunDatar();
        System.out.print("masukan nilai n: ");
        bangunDatar.cetak();
    }
}
