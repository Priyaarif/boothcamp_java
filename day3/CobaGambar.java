package day3;

public class CobaGambar {

    NilaiN nilai = new NilaiN();
    private int n = nilai.nilaiN();

    public void buatGaris() {
        int n = this.n;
        for (int i = 0; i <= n*(n-1); i++) {
            System.out.print("X");
        }
    }

    public void buatUjung() {
        int n = this.n;
        for (int i = 0; i <= n*(n-1); i++) {
            if (i%(n-1)==0){
                System.out.print("X");
            }
            else {
                System.out.print(" ");
            }
        }
    }
}
