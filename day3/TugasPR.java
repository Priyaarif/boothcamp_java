package day3;

import java.util.Scanner;

public class TugasPR {
	
	private int a;
	private int b;
	
	public int getA(){
		return a;
	}
	
	public void setA(int newA){
		this.a = newA;
	}
	
	public int getB(){
		return b;
	}
	
	public void setB(int newB){
		this.b = newB;
	}
		
	public void gambar(){
		int n;
		Scanner data = new Scanner(System.in);
		n = data.nextInt();
		setA(1);
		setB(1);
		int c = 0;
		for (int i = 0; i < n; i++) {
			if (i % 4 == 2){
				c = c + n - 1;
			} else if (i % 4 == 3){
				c = c + n + 1;
			}
			for (int j = 0; j < n; j++) {
				int a = getA();
				int b = getB();
				if (i%4==0) {
//					System.out.printf("%3s",a);
					System.out.printf("%3s",c++);
					setA(a+1);
				}
				else if (i%4==2) {
//					System.out.printf("%3s",a);
					System.out.printf("%3s",c--);
					setB(a+n);
					setA(a-1);
				}
				else if (i%4==1) {
					if (j==n-1) {
//						System.out.printf("%3s",a);
						System.out.printf("%3s",c++);
						setA(a+n);
					}
					else {
						System.out.printf("%3s"," ");
					}
				}
				else if (i%4==3) {
					if (j==0) {
//						System.out.printf("%3s",b);
						System.out.printf("%3s",c++);
						setA(b+1);

					}
					else {
						System.out.printf("%3s"," ");
					}
				}
			}
			System.out.println();
		}
	}
}