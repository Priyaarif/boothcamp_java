package day3;

public class Cetak {

    NilaiN nilai = new NilaiN();
    int n = nilai.nilaiN();

    public void cetakGambar(){
        int n = this.n;
        CobaGambar cetakin = new CobaGambar();
        for (int i = 0; i < n; i++){
            if (i==0 || i==n-1){
                cetakin.buatGaris();
            }
            else {
                cetakin.buatUjung();
            }
            System.out.println();
        }
    }
}
