package day3;

import java.util.Scanner;

public class BelajarMethod2 {

    public String nama(){
        Scanner data = new Scanner(System.in);
        String nama = data.nextLine();
        return nama;
    }

    public int umur(){
        Scanner data = new Scanner(System.in);
        int umur = data.nextInt();
        return umur;
    }
}
