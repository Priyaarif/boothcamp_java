package day6;

import java.util.Arrays;
import java.util.Scanner;

public class Tugas1 {

    public static void main(String[] args) {
        Tugas1 tugas = new Tugas1();
        tugas.print();
    }

    public int nilaiN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public int[] arrayGanjil(int n){
        int[] s = new int[n];
        int j = 0;
        for (int i = 0; i < n*2 ; i++){
            if (i % 2 == 1){
                s[j] = i;
                j++;
            }
        }
        return s;
    }

    public void print(){
        Tugas1 tugas = new Tugas1();
        int n = tugas.nilaiN();
        int[] s = tugas.arrayGanjil(n);
        for (int i = 0; i < n ; i++){
            System.out.print(s[i]+" ");
        }
        System.out.println();
        System.out.print(Arrays.toString(s));
    }
}
