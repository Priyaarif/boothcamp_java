package day6;

import java.util.Arrays;
import java.util.Scanner;

public class Kuis1 {

    public static void main(String[] args) {
        Kuis1 kuis = new Kuis1();
        kuis.print();
    }

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public char getA(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan char a : ");
        char a = data.next().charAt(0);
        return a;
    }

    public char[] arrayChar(int n, char a){
        char[] a2D = new char[n];
        for (int i = 0; i < n ; i++) {
            a2D[i] = a++;
        }
        return a2D;
    }

    public int[] arrayInt(int n){
        int[] a2D = new int[n];
        for (int i = 0; i < n ; i++) {
            a2D[i] = i * 2 + 1;
        }
        return a2D;
    }

    public String[][] save(int n){
        String[][] savechar = new String[n][n];
        return savechar;
    }

    public void print(){
        int n = getN();
        char a = getA();
        int[] x = arrayInt(n);
        char[] y = arrayChar(n,a);
        String[][] z = save(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i % 2 == 0 && j < n/2) {
                    z[i][j] = String.valueOf(y[j]);
                    System.out.printf("%3s", z[i][j]);
                } else if ( i % 2 == 1 && j < n/2){
                    z[i][j] = String.valueOf(x[j]);
                    System.out.printf("%3s", x[j]);
                } else if ( i % 2 == 0 && j >= n/2){
                    z[i][j] = String.valueOf(y[n-j-1]);
                    System.out.printf("%3s", y[n-j-1]);
                } else if ( i % 2 == 1 && j >= n/2){
                    z[i][j] = String.valueOf(x[n-j-1]);
                    System.out.printf("%3s", x[n-j-1]);
                }
            }
            System.out.println();
        }
        System.out.println(Arrays.deepToString(z));
    }
}
