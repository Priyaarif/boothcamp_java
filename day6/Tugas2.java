package day6;

import java.util.Arrays;

public class Tugas2 {

    public static void main(String[] args) {
        Tugas2 tugas = new Tugas2();
        tugas.print();

    }

    public int[][] array2D(){
        int[][] a2D = {
                {1, 2, 3},
                {4, 5, 6, 7},
                {8, 9},
        };
        return a2D;
    }

    public void print(){
        int[][] a = array2D();
        System.out.println(a[0].length);
        System.out.println(a[1].length);
        System.out.println(a[2].length);
    }
}
