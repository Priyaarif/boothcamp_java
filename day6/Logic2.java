package day6;

import java.util.Scanner;

public class Logic2 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public char getA(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan char : ");
        char a = data.next().charAt(0);
        return a;
    }

    public int soalY(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan soal yang dipilih : ");
        int y = data.nextInt();
        return y;
    }

    public int[] ganjil(int n){
        int[] array = new int[n];
        for (int i = 0; i < n ; i++){
            array[i] = i*2+1;
        }
        return array;
    }

    public int[] genap(int n){
        int[] array = new int[n];
        for (int i = 0; i < n ; i++){
            array[i] = i*2;
        }
        return array;
    }

    public char[] arrayChar(int n,char a){
        char[] array = new char[n];
        for (int i = 0; i < n ; i++){
            array[i] = a++;
        }
        return array;
    }

    public void soal1(int n){
        int[] array = ganjil(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++) {
                if (i == j) {
                    System.out.printf("%3s",array[i]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal2(int n){
        int[] array = genap(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i+j == (n-1)){
                    System.out.printf("%3s",array[i]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal3(int n){
        int[] ganjil = ganjil(n);
        int[] genap = genap(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i == j){
                    System.out.printf("%3s",ganjil[i]);
                } else if (i+j == n-1){
                    System.out.printf("%3s",genap[n-i-1]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal4(int n){
        int[] ganjil = ganjil(n);
        int[] genap = genap(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i == j || j == n/2){
                    System.out.printf("%3s",ganjil[i]);
                } else if ( i+j == n-1 ){
                    System.out.printf("%3s",genap[n-i-1]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal5(int n){
        int[] ganjil = ganjil(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (j <= i){
                    System.out.printf("%3s",ganjil[j]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal6(int n){
        int[] genap = genap(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i+j>=n-1){
                    System.out.printf("%3s",genap[j]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal7(int n, char a) {
        int[] ganjil = ganjil(n);
        int[] genap = genap(n);
        char[] b = arrayChar(n,a);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) {
                    System.out.printf("%3s", ganjil[i]);
                } else if (i + j == n - 1) {
                    System.out.printf("%3s", genap[n - i - 1]);
                } else if (i < j && i + j < n - 1) {
                    System.out.printf("%3s", b[0]);
                } else if (i > j && i + j >= n - 1) {
                    System.out.printf("%3s", b[1]);
                } else {
                    System.out.printf("%3s", " ");
                }
            }
            System.out.println();
        }
    }

    public void soal8(int n, char a){
        int[] ganjil = ganjil(n);
        int[] genap = genap(n);
        char[] charA = arrayChar(n,a);
        for ( int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if ( i == j) {
                    System.out.printf("%3s", ganjil[i]);
                } else if (i+j == n-1){
                    System.out.printf("%3s",genap[n-j-1]);
                } else if ( i > j && i+j < n-1){
                    System.out.printf("%3s",charA[0]);
                } else if ( i < j && i+j >= n-1){
                    System.out.printf("%3s",charA[1]);
                } else {
                    System.out.printf("%3s"," ");
                }
            }
            System.out.println();
        }
    }

    public void soal9(int n){
        int[] ganjil = ganjil(n);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (j <= n/2){
                    System.out.printf("%3s",ganjil[j]);
                } else {
                    System.out.printf("%3s",ganjil[n-j-1]);
                }
            }
            System.out.println();
        }
    }

    public void soal10(int n){
        int[] genap = genap(n);
        for ( int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                if (i <= n/2) {
                    System.out.printf("%3s",genap[i]);
                } else {
                    System.out.printf("%3s",genap[n-i-1]);
                }
            }
            System.out.println();
        }
    }

    public void looping(){
        Scanner data = new Scanner(System.in);
        String loop = null;
        do {
            int y = soalY();
            pilihan(y);
            System.out.println("apakah ingin berhenti? (y/n)");
            loop = data.nextLine();
        } while (loop.equals("n"));
    }

    public void pilihan(int y){
        int n = getN();
        switch (y){
            case 1 :
                soal1(n);
                break;
            case 2 :
                soal2(n);
                break;
            case 3 :
                soal3(n);
                break;
            case 4 :
                soal4(n);
                break;
            case 5 :
                soal5(n);
                break;
            case 6 :
                soal6(n);
                break;
            case 7 :
                char a = getA();
                soal7(n,a);
                break;
            case 8 :
                char b = getA();
                soal8(n,b);
                break;
            case 9 :
                soal9(n);
                break;
            case 10 :
                soal10(n);
                break;
            default :
                System.out.print("pilihan soal yang anda masukan tidak terdaftar");
                break;
        }
    }

    public static void main(String[] args) {
        Logic2 logic2 = new Logic2();
        logic2.looping();
    }

}
