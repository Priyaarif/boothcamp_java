package day8;

import java.util.Arrays;
import java.util.Scanner;

public class Tugas1 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public int getSoal(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nomor soal : ");
        int y = data.nextInt();
        return y;
    }

    public int[] getGanjil(int n){
        int[] x = new int[n];
        int k = 0;
        if ( n <= 2){
            System.out.println("n yang anda masukan kurang dari 2");
        } else {
            for (int i = 0; i < n * 2; i++) {
                if (i % 2 == 1) {
                    x[k++] = i;
                }
            }
        }
        return x;
    }

    public int[] getInput(int n){
        int[] k = new int[n];
        Scanner data = new Scanner(System.in);
        System.out.println("masukan nilai yang ingin di sort : ");
        for (int i = 0; i < n ; i++){
            k[i] = data.nextInt();
        }
        return k;
    }

    public int[] soal1(int n){
        int[] fib = new int[n];
        fib[0] = fib[1] = 1;
        for (int i = 2; i < n ; i++){
            fib[i] = fib[i-2] + fib[i-1];
        }
        return fib;
    }

    public int[] soal2(int n){
        int[] g = new int[n];
        g[n-1] = g[n-2] = 1;
        for (int i = 3; i <= n ; i++){
            g[n-i] = g[n+1-i] + g[n+2-i];
        }
        return g;
    }

    public void soal3(int n){
        int[] input = getInput(n);
        int x;
        for (int i = 1; i < input.length ; i++){
            for (int j = i; j > 0 ; j--){
                if (input[j] <= input[j-1]){
                    x = input[j];
                    input[j] = input[j-1];
                    input[j-1] = x;
                }
            }
        }
        System.out.println(Arrays.toString(input));
    }

    public void soal4(int n){
        int[] input = getInput(n);
        int x;
        for (int i = 1; i < input.length ; i++){
            for (int j = i; j > 0 ; j--){
                if (input[j] >= input[j-1]){
                    x = input[j];
                    input[j] = input[j-1];
                    input[j-1] = x;
                }
            }
        }
        System.out.println(Arrays.toString(input));
    }

    public void soal5(int n){
        char a = 'A';
        int y = 0;
        int[] fib = soal1(n);
        for (int i = 0; i < n ; i++){
            if (i % 2 == 1){
                System.out.printf("%3d", fib[y++]);
            } else {
                System.out.printf("%3s", a++);
            }
        }
        System.out.println();
    }

    public void soal6(int n){
        int[] ganjil = getGanjil(n);
        System.out.println(ganjil[1]+" * "+ganjil[n-1]+" = "+ganjil[1]*ganjil[n-1]);
    }

    public void pilihan(int y){
        int n = getN();
        System.out.println("Jawaban soal nomor "+y+" : ");
        switch (y){
            case 1 :
                System.out.println(Arrays.toString(soal1(n)));
                break;
            case 2 :
                System.out.println(Arrays.toString(soal2(n)));
                break;
            case 3 :
                soal3(n);
                break;
            case 4 :
                soal4(n);
                break;
            case 5 :
                soal5(n);
                break;
            case 6 :
                soal6(n);
                break;
            default :
                System.out.println("nomor yang anda masukan belum terdaftar");
        }
    }

    public void looping(){
        Scanner data = new Scanner(System.in);
        String loop = null;
        do {
            int y = getSoal();
            pilihan(y);
            System.out.println("apakah ingin berhenti? (y/n) ");
            loop = data.nextLine();
            System.out.println("---------------------------------------------");
        } while (loop.equals("n"));
    }

    public static void main(String[] args) {
        Tugas1 tugas = new Tugas1();
        tugas.looping();
    }
}
