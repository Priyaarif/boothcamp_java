import java.util.Scanner;

public class MateriLogicSoalTujuh {
	public static void main(String[] args) {
		MateriLogicSoalTujuh cobaCetak = new MateriLogicSoalTujuh();
		System.out.print("masukan nilai n : ");
		cobaCetak.cetak();
	}

	public int getA(){
		int n;
		Scanner data = new Scanner(System.in);
		n = data.nextInt();
		return n;
	}

	public void cetak(){
		int n = getA();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				//System.out.print("*");
				if (i >= n-j) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}