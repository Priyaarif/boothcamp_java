import java.util.Scanner;

public class LatihanBintang {
	public static void main(String[] args) {
		int n;
		Scanner data = new Scanner(System.in);
		System.out.print("Masukan nilai n : ");
		n = data.nextInt();
		for (int i = 0; i < n; i++) {
			System.out.printf("%2s","*");
		}
	}
}