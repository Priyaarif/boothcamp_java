package day2;

import java.util.Scanner;

public class KuisSoal1 {
	public static void main(String[] args) {
		int  n;
		Scanner data = new Scanner(System.in);
		System.out.print("masukan nilai n : ");
		n = data.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (i%2==0) {
					System.out.print("X");
				}
				else if (i%4==1) {
					if (j==n-1) {
						System.out.print("-");
					}
					else {
						System.out.print(" ");
					}
				}
				else if (i%4==3) {
					if (j==0) {
						System.out.print("-");
					}
					else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}
	}
}