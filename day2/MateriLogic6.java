import java.util.Scanner;

public class MateriLogic6 {
	public static void main(String[] args) {
		int n;
		Scanner data = new Scanner(System.in);
		System.out.print("masukan nilai n : ");
		n = data.nextInt();
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print("*");
				if (i == j) {
					System.out.println();
					break;
				}
			}
		}
	}
}	