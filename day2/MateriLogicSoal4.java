import java.util.Scanner;

public class MateriLogicSoal4 {
	public static void main(String[] args) {
		int n = 9;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				//System.out.print(" ");
				if (i==j || j==4 || j==n-i-1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}