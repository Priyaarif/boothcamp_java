import java.util.Scanner;

public class MateriLogicSoal5 {
	public static void main(String[] args) {
		int n  = 9;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				//System.out.print(" ");
				if (i==j || j==n-i-1 || i==0 || j==0 || i==n-1 || j==n-1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}
}