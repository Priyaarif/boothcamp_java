package ujian;

import java.util.Scanner;

public class nomer2 {

    public String getInput(){
        Scanner data = new Scanner(System.in);
        System.out.println("Input : ");
        String inp = data.nextLine();
        return inp;
    }

    public void cetak(){
        String inp = getInput();
        char[] iChar = inp.toCharArray();
        System.out.println("Output : ");
        int x = 0;
        for (int i = 0; i < iChar.length/2 ; i++){
            if(iChar[i] != iChar[iChar.length-i-1]){
                System.out.println("NO");
                x++;
                break;
            }
        }
        if (x != 1){
            System.out.println("YES");
        }
    }

    public static void main(String[] args) {
        nomer2 dua = new nomer2();
        dua.cetak();

    }
}
