package ujian;

import java.util.Scanner;

public class nomer9 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("n = ");
        int n = data.nextInt();
        return n;
    }

    public int[] getDeret3(int n){
        int[] deret3 = new int[n];
        for (int i = 1; i <= n ; i++){
            deret3[i-1] = i*3;
        }
        return deret3;
    }

    public int[] getDeret2(int n){
        int[] deret2 = new int[n];
        for (int i = 1; i <= n ; i++){
            deret2[i-1] = i*2;
        }
        return deret2;
    }

    public void cetak(){
        int n = getN();
        int[] d3 = getDeret3(n);
        int[] d2 = getDeret2(n);
        int y = 0;
        int x = 0;
        for(int i = 0; i < n ; i++){
            for(int j = 0; j < n ; j++) {
                if (i % 2 == 0) {
                    if (i < j && i + j < n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i > j && i + j > n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i < j && i + j > n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i > j && i + j < n-1) {
                        System.out.printf("%3s"," ");
                    } else {
                        System.out.printf("%3d",d3[x]);
                    }
                } else {
                    if (i < j && i + j < n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i > j && i + j > n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i < j && i + j > n-1) {
                        System.out.printf("%3s"," ");
                    } else if (i > j && i + j < n-1) {
                        System.out.printf("%3s"," ");
                    } else {
                        System.out.printf("%3s",d2[y]);
                    }
                }
            }
            if(i % 2 == 0){
                x++;
            } else {
                y++;
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        nomer9 sembilan = new nomer9();
        sembilan.cetak();
    }
}
