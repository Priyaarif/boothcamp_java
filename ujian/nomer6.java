package ujian;

import java.util.Arrays;
import java.util.Scanner;

public class nomer6 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("n = ");
        int n = data.nextInt();
        return n;
    }

    public char[][] getArr(int n){
        char[][] arr = new char[n][n];
        for (int i = 0; i < n ; i++){
            for(int j = 0; j < n ; j++){
                if (i == n-1 && j == n-1) {
                    arr[n - 1][n - 1] = '0';
                //} else if(i == 0 && j == n-1){
                  //  arr[i][j] = " ";
                } else if(i >= n-j-1){
                    arr[i][j] = '*';
                } else {
                    arr[i][j] = ' ';
                }
            }
        }
        return arr;
    }

//    public char[][] getArr2(int n, char[][] arr){
//        int y = 0;
//        char[][] arr2 = new char[n][n*3];
//        do{
//            for (int i = 0; i < n ; i++){
//                for (int j = 0; j < n ; i++){
//                    arr2[i][j+y] = arr[i][j];
//                }
//            }
//            y+=n-1;
//        } while (y < n*3);
//        return arr2;
//    }

    public void cetak(){
        int n = getN();
        char[][] arr = getArr(n);
        //char[][] arr2 = getArr2(n, arr);
        for (int i = 0; i < n ; i++){
            for (int j = 0; j < n ; j++){
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        nomer6 enam = new nomer6();
//        int n = enam.getN();
//        char[][] arr = enam.getArr(n);
//        char[][] arr2 = enam.getArr2(n,arr);
        enam.cetak();
    }
}
