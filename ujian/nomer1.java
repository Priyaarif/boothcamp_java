package ujian;

import java.util.Scanner;

public class nomer1 {

    public int getUang(){
        Scanner data = new Scanner(System.in);
        System.out.print("Uang Andi : ");
        int n = data.nextInt();
        return n;
    }

    public int[] getHargaKacamata(){
        Scanner data = new Scanner(System.in);
        System.out.println("Harga Kacamata :");
        int[] hk = new int[3];
        for (int i = 0; i < 3 ; i++){
            hk[i] = data.nextInt();
        }
        return hk;
    }

    public int[] getBaju(){
        Scanner data = new Scanner(System.in);
        System.out.println("Harga Baju :");
        int[] b = new int[3];
        for (int i = 0; i < 3 ; i++){
            b[i] = data.nextInt();
        }
        return b;
    }

    public int[] getBeli(){
        int uang = getUang();
        int[] km = getHargaKacamata();
        int[] bj = getBaju();
        int x = 0;
        int k = 3;
        int[] saveInt = new int[9];
        for (int i = 0; i < k ; i++){
            for (int j = 0; j < k ; j++){
                if (km[i] + bj[j] <= uang) {
                    saveInt[x] = km[i] + bj[j];
                    x++;
                }
            }
        }
        return saveInt;
    }

    public void cetak(){
        int[] beli = getBeli();
        int maksimum = beli[0];
        for (int i = 1 ; i < beli.length ; i++){
            if(beli[i] > maksimum){
                maksimum = beli[i];
            }
        }
        if (maksimum == 0){
            System.out.println("Dana tidak mencukupi");
        } else {
            System.out.print("maksimal uang : " + maksimum);
        }
    }

    public static void main(String[] args) {
        nomer1 satu = new nomer1();
        satu.cetak();
    }
}
