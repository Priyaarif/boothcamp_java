package ujian;

import java.util.Scanner;

public class nomer10 {

    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("n = ");
        int n = data.nextInt();
        return n;
    }

    public void cetak(){
        int n = getN();
        for (int i = 0 ; i < n ; i++){
            for (int j = 0 ; j < n ; j++){
                if (i == 0 && j == 0){
                    System.out.printf("%3s"," ");
                }else if (i == n-1 && j == 0){
                    System.out.printf("%3s"," ");
                }else if (i == 0 && j == n-1){
                    System.out.printf("%3s"," ");
                }else if (i == n-1 && j == n-1){
                    System.out.printf("%3s"," ");
                }else if (i % 2 == 0){
                    System.out.printf("%3d",i);
                }else{
                    System.out.printf("%3s","*");
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        nomer10 sp = new nomer10();
        sp.cetak();
    }
}
