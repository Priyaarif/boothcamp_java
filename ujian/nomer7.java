package ujian;

import java.util.Scanner;

public class nomer7 {

    public int getX(){
        Scanner data = new Scanner(System.in);
        System.out.print("Bila X = ");
        int x = data.nextInt();
        return x;
    }

    public int[] ganjil(int x){
        int[] gj = new int[x];
        int y = 0;
        for (int i = 0 ; i < x*2 ; i++){
            if(i % 2 == 0){
                    gj[y] = i;
                    y++;
            }
        }
        return gj;
    }

    public void cetak(){
        int x = getX();
        int[] gj = ganjil(x);
        for (int i = 0 ; i < 2 ; i++){
            for (int j = 0 ; j < x ; j++){
                if(i == 0) {
                    System.out.printf("%3s", gj[j]);
                } else {
                    if (gj[x-j-1] % 3 == 0){
                        System.out.printf("%3s","-");
                    } else {
                        System.out.printf("%3s", gj[x - j - 1]);
                    }
                }
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        nomer7 tujuh = new nomer7();
        tujuh.cetak();
    }
}
