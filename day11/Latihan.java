package day11;

import java.util.Scanner;

public class Latihan {


    public int getN(){
        Scanner data = new Scanner(System.in);
        System.out.print("masukan nilai n : ");
        int n = data.nextInt();
        return n;
    }

    public int[] setArr(int n){
        int[] arr = new int[n];
        Scanner data = new Scanner(System.in);
        System.out.println("masukan array yang  di mau (0/1) : ");
        for (int i= 0; i < n; i++){
            arr[i] = data.nextInt();
        }
        return arr;
    }

    public void lompat(){
        int n = getN();
        int[] arr = setArr(n);
        int y = 0;
        for (int i = 0; i < n-2 ; i++) {
            menu:
            for (int j = i; j < n-1 ; j++){
                if (arr[j+1]==1){
                    if (arr[j+2]==0){
                        y++;
                        i = i + j + 1;
                        continue menu;
                    } else {
                        y++;
                        i = i + j;
                        continue  menu;
                    }
                } else {
                    y++;
                    i++;
                    continue menu;
                }
            }

        }
        System.out.println(y);
    }

    public static void main(String[] args) {
        Latihan latih = new Latihan();
        latih.lompat();
    }
}
